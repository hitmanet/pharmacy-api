const mongoose = require('mongoose')
const User = mongoose.model('User')

const api = {}

// api.index = (User, Token) => (req, res) => {
//     const token = Token
//     if (token){
//         User.find({}, (error, users) => {
//             if (error) throw error
//             res.status(200).json(users)
//         })
//     } else {
//         return res.status(403).send('Not auth')
//     }
// }

api.signup = () => (req, res) => {
    if (!req.body.username || !req.body.password)
        return res.json('Pass email and password')
    else {
        const newUser = new User({
            username: req.body.username,
            password: req.body.password
        })
        newUser.save(error => {
            if (error)
                res.status(400).send('Username already exists')
            res.json({
                success: true,
                newUser
            })
        })
    }
}

module.exports = api