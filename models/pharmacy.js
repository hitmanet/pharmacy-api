const mongoose = require('mongoose')


const pharmacySchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    medication: [{
        medicine: {
            ref: 'Medicine',
            type: mongoose.Schema.Types.ObjectId,
        },
        count: {
            type: Number,
            default: 0
        }
    }]
})

mongoose.model('Pharmacy', pharmacySchema)