const mongoose = require('mongoose')


const medicineSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    pharmacies: [{
        ref: 'Pharmacy',
        type: mongoose.Schema.Types.ObjectId
    }]
})

mongoose.Schema('Medicine', medicineSchema)