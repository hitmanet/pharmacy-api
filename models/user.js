const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const userSchema = mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

userSchema.pre('save', function(next){
    const user = this
    if (this.isNew){
        bcrypt.genSalt(10, (err, salt) => {
            if (err)
                return err
            bcrypt.hash(user.password, salt, (error, hash) => {
                if (error)
                    return error
                user.password = hash
                next()
            })
        })
    } else return next()
})

userSchema.methods.comparePassword = function(password, callback) {
    bcrypt.compare(password, this.password, (error, matches) => {
        if (error)
            return error
        callback(null, matches)
    })
}

mongoose.model('User', userSchema)