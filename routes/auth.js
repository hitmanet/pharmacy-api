const authApi = require('../api/auth')

module.exports = (app) => {
    app.route('/api/v1/auth').post(authApi.login())
}