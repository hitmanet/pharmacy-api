const passport = require('passport-jwt')
const config = require('@config')
const userApi = require('../api/user')

module.exports = (app) => {
    app.route('/api/v1/signup')
        .post(userApi.signup())
}