require('module-alias/register')

const http = require('http')
const Api = require('@Api')
const ApiServer = http.Server(Api)
const port = 3001
const LOCAL = '0.0.0.0'

ApiServer.listen(port, LOCAL, () => console.log('app is running'))