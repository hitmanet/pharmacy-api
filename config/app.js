const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const morgan = require('morgan')
const cors = require('cors')
const passport = require('passport')
const consign = require('consign')
const jwt = require('jsonwebtoken')
const passportConfig = require('./passport')(passport)
const config = require('./index')
const db = require('./db')(mongoose, config)

app.use(express.static('.'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(passport.initialize())

app.set('apisecret', config.secret)

const UserRouter = require('../routes/user')(app)
const AuthRouter = require('../routes/auth')(app)


module.exports = app